/* Copyright 2022 Jaakko Keränen <jaakko.keranen@iki.fi>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

#pragma once

#include "SDL_surface.h"

#include <the_Foundation/array.h>
#include <the_Foundation/rect.h>
#include <the_Foundation/string.h>
#include <curses.h>

iDeclareType(Canvas)
iDeclareType(VisChar)

enum iVisCharAttribute {
    none_VisCharAttribute      = 0,
    bold_VisCharAttribute      = iBit(1),
    italic_VisCharAttribute    = iBit(2),
    underline_VisCharAttribute = iBit(3),
    reverse_VisCharAttribute   = iBit(4),
    blink_VisCharAttribute     = iBit(5),
};

#define transparent_VisChar     0xff

struct Impl_VisChar {
    iChar      ch; /* TODO: Multi-codepoint sequences? Add a longer buffer? */
    uint8_t    fg;
    uint8_t    bg;
    uint16_t   attr;
};

struct Impl_Canvas {
    iInt2  size;
    iRect  clipRect;
    iArray buf;
};

iDeclareTypeConstructionArgs(Canvas, iInt2 size)

void    resize_Canvas       (iCanvas *, iInt2 size);
void    setClip_Canvas      (iCanvas *, iRect rect);
void    unsetClip_Canvas    (iCanvas *);
void    blit_Canvas         (iCanvas *, const iCanvas *src, iRect srcRect, iInt2 dst, SDL_BlendMode blend);
void    clear_Canvas        (iCanvas *, int color);
void    setChar_Canvas      (iCanvas *, iInt2 pos, iChar ch, int color);
void    fillRect_Canvas     (iCanvas *, iRect rect, int color);
void    fillRectChar_Canvas (iCanvas *, iRect rect, int bgColor, iChar ch, int fgColor);
void    drawRect_Canvas     (iCanvas *, iRect rect, int color);
void    present_Canvas      (const iCanvas *, WINDOW *cwin);

iRect   clipBounds_Canvas   (const iCanvas *);

iVisChar *      at_Canvas       (iCanvas *, iInt2 pos);
const iVisChar *constAt_Canvas  (const iCanvas *, iInt2 pos);
