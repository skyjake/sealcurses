/* Copyright 2022 Jaakko Keränen <jaakko.keranen@iki.fi>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

#include "SDL.h"
#include "events.h"
#include "timer.h"

#include <the_Foundation/defs.h>

int SDL_Init(int flags) {
    return SDL_InitSubSystem(flags);
}

int SDL_InitSubSystem(int flags) {
    init_Foundation();
    SDL_InitTimer();
    SDL_InitEvents();
    return 0;
}

void SDL_Quit(void) {
    SDL_QuitEvents();
    SDL_QuitTimer();
    deinit_Foundation();
}

const char *SDL_GetError(void) {
    return "";
}

void SDL_free(void *p) {
    free(p);
}

char *SDL_GetBasePath(void) {
    return NULL;
}

char *SDL_GetPrefPath(const char *org, const char *app) {
    iAssert(iFalse);
    return NULL;
}

int SDL_OpenURL(const char *url) {
    return -1;
}
