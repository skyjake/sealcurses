/* Copyright 2022 Jaakko Keränen <jaakko.keranen@iki.fi>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

#include "SDL_keyboard.h"
#include "video.h"

#include <curses.h>
#include <the_Foundation/vec2.h>

static iBool isTextInputMode_;
static iInt2 insertionPos_;
static int modState_;

void SDL_SetModState(int modState) {
    modState_ = modState;
}

void SDL_ShowCursor(void) {
    wmove(currentWindow_Video->cwin, insertionPos_.y, insertionPos_.x);
}

SDL_Keymod SDL_GetModState(void) {
    return modState_;
}

const char *SDL_GetKeyName(int sym) {
    static const struct { int s; const char *name; } names[] = {
        { SDLK_HOME, "Home" },
        { SDLK_END, "End" },
        { SDLK_PAGEUP, "PgUp" },
        { SDLK_PAGEDOWN, "PgDn" },
        { SDLK_TAB, "TAB" },
        { SDLK_F1, "F1" },
        { SDLK_F2, "F2" },
        { SDLK_F3, "F3" },
        { SDLK_F4, "F4" },
        { SDLK_F5, "F5" },
        { SDLK_F6, "F6" },
        { SDLK_F7, "F7" },
        { SDLK_F8, "F8" },
        { SDLK_F9, "F9" },
        { SDLK_F10, "F10" },
        { SDLK_F11, "F11" },
        { SDLK_F12, "F12" },
    };
    iForIndices(i, names) {
        if (names[i].s == sym) {
            return names[i].name;
        }
    }
    return "";
}

void SDL_StartTextInput(void) {
    /* We don't show the cursor right away because it may not be at the right position. */
    isTextInputMode_ = iTrue;
    leaveok(currentWindow_Video->cwin, false);
}

void SDL_StopTextInput(void) {
    isTextInputMode_ = iFalse;
    curs_set(0);
    leaveok(currentWindow_Video->cwin, true);
}

int SDL_IsTextInputStarted(void) {
    return isTextInputMode_;
}

void SDL_SetTextInputRect(const SDL_Rect *rect) {    
    insertionPos_ = init_I2(rect->x, rect->y);
}
