/* Copyright 2022 Jaakko Keränen <jaakko.keranen@iki.fi>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

#include "SDL_surface.h"

#include <the_Foundation/array.h>

struct SDL_Palette_s {
    iArray colors;
};

SDL_Palette *SDL_AllocPalette(int ncolors) {
    SDL_Palette *d = malloc(sizeof(SDL_Palette));
    init_Array(&d->colors, sizeof(SDL_Color));
    return d;
}

void SDL_FreePalette(SDL_Palette *d) {
    deinit_Array(&d->colors);
    free(d);
}

void SDL_SetPaletteColors(SDL_Palette *d, const SDL_Color *colors, int first, int ncolors) {
    setN_Array(&d->colors, first, colors, ncolors);
}

const char *SDL_GetPixelFormatName(int format) {
    return "";
}

SDL_Surface *SDL_CreateRGBSurfaceWithFormatFrom(void *pixels, int width, int height, int depth,
                                                int pitch, int format) {
    SDL_Surface *d = malloc(sizeof(SDL_Surface));
    d->flags = 0;
    d->w = width;
    d->h = height;
    d->blendMode = SDL_BLENDMODE_NONE;
    d->pixels = NULL;
    return d;
}

void SDL_FreeSurface(SDL_Surface *d) {
    if (d) {
        free(d);
    }
}

void SDL_SetSurfaceBlendMode(SDL_Surface *d, SDL_BlendMode mode) {
    d->blendMode = mode;
}
