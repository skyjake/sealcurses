/* Copyright 2022 Jaakko Keränen <jaakko.keranen@iki.fi>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

#pragma once

#include "SDL_types.h"

typedef struct SDL_Renderer_s SDL_Renderer;

enum {
    SDL_WINDOW_HIDDEN = 0x1,
    SDL_WINDOW_RESIZABLE = 0,
    SDL_WINDOW_OPENGL = 0,
    SDL_WINDOW_ALLOW_HIGHDPI = 0,
    SDL_WINDOW_FULLSCREEN_DESKTOP = 0,
    SDL_WINDOW_MINIMIZED = 0,
    SDL_WINDOW_MAXIMIZED = 0,
    SDL_WINDOW_ALWAYS_ON_TOP = 0,
    SDL_WINDOW_BORDERLESS = 0,
    SDL_WINDOW_POPUP_MENU = 0,
    SDL_WINDOW_SKIP_TASKBAR = 0,
    SDL_WINDOW_INPUT_FOCUS = 0,
};

enum {
    SDL_WINDOWPOS_CENTERED = -1000000
};

enum {
    SDL_WINDOWEVENT_CLOSE,
    SDL_WINDOWEVENT_EXPOSED,
    SDL_WINDOWEVENT_SHOWN,
    SDL_WINDOWEVENT_RESTORED,
    SDL_WINDOWEVENT_RESIZED,
    SDL_WINDOWEVENT_SIZE_CHANGED,
    SDL_WINDOWEVENT_MOVED,
    SDL_WINDOWEVENT_FOCUS_LOST,
    SDL_WINDOWEVENT_FOCUS_GAINED,
    SDL_WINDOWEVENT_LEAVE,
    SDL_WINDOWEVENT_ENTER,
    SDL_WINDOWEVENT_MAXIMIZED,
    SDL_WINDOWEVENT_MINIMIZED,
    SDL_WINDOWEVENT_TAKE_FOCUS,
    SDL_WINDOWEVENT_DISPLAY_CHANGED,
};

typedef struct SDL_Window_s SDL_Window;

int         SDL_GetNumVideoDisplays     (void);
void        SDL_EnableScreenSaver       (void);

SDL_Window *SDL_CreateWindow            (const char *title, int x, int y, int w, int h, int flags);
void        SDL_DestroyWindow           (SDL_Window *);
uint32_t    SDL_GetWindowID             (SDL_Window *);
void        SDL_ShowWindow              (SDL_Window *);
void        SDL_HideWindow              (SDL_Window *);
void        SDL_MaximizeWindow          (SDL_Window *);
void        SDL_MinimizeWindow          (SDL_Window *);
void        SDL_RestoreWindow           (SDL_Window *);
void        SDL_RaiseWindow             (SDL_Window *);
int         SDL_GetWindowDisplayIndex   (SDL_Window *);
uint32_t    SDL_GetWindowFlags          (SDL_Window *);
void        SDL_GetDisplayBounds        (int index, SDL_Rect *bounds);
void        SDL_GetDisplayUsableBounds  (int index, SDL_Rect *bounds);
void        SDL_GetWindowPosition       (SDL_Window *, int *x, int *y);
void        SDL_GetWindowSize           (SDL_Window *, int *w, int *h);
void        SDL_SetWindowFullscreen     (SDL_Window *, int mode);
void        SDL_SetWindowInputFocus     (SDL_Window *);
void        SDL_SetWindowMinimumSize    (SDL_Window *, int w, int h);
void        SDL_SetWindowPosition       (SDL_Window *, int x, int y);
void        SDL_SetWindowSize           (SDL_Window *, int w, int h);
void        SDL_SetWindowTitle          (SDL_Window *, const char *title);
