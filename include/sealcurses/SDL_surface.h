/* Copyright 2022 Jaakko Keränen <jaakko.keranen@iki.fi>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

#pragma once

#include "SDL_types.h"

enum {
    SDL_PREALLOC = 0x1,
};

typedef struct SDL_Surface_s {
    int w;
    int h;
    int flags;
    int blendMode;
    void *pixels;
} SDL_Surface;

typedef enum {
    SDL_BLENDMODE_NONE,
    SDL_BLENDMODE_BLEND,
    SDL_BLENDMODE_ADD,
} SDL_BlendMode;

typedef struct SDL_Palette_s SDL_Palette;

SDL_Palette *   SDL_AllocPalette        (int ncolors);
void            SDL_FreePalette         (SDL_Palette *);
void            SDL_SetPaletteColors    (SDL_Palette *, const SDL_Color *colors, int first, int ncolors);

enum {
    SDL_PIXELFORMAT_INDEX8,
    SDL_PIXELFORMAT_RGB888,
    SDL_PIXELFORMAT_RGBA4444,
    SDL_PIXELFORMAT_RGBA32,
    SDL_PIXELFORMAT_RGBA8888,
    SDL_PIXELFORMAT_ABGR8888,
};

const char *SDL_GetPixelFormatName(int format);

enum {
    SDL_TEXTUREACCESS_STATIC = 0x1,
    SDL_TEXTUREACCESS_TARGET = 0x2,
};

SDL_Surface *SDL_CreateRGBSurfaceWithFormatFrom
                                (void *pixels, int width, int height, int depth, int pitch, int format);
void    SDL_FreeSurface         (SDL_Surface *);

void    SDL_SetSurfacePalette   (SDL_Surface *, SDL_Palette *pal);
void    SDL_SetSurfaceBlendMode (SDL_Surface *, SDL_BlendMode mode);
