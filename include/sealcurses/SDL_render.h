/* Copyright 2022 Jaakko Keränen <jaakko.keranen@iki.fi>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

#pragma once

#include "SDL_surface.h"
#include "SDL_types.h"

typedef struct SDL_Window_s     SDL_Window;
typedef struct SDL_Renderer_s   SDL_Renderer;
typedef struct SDL_Texture_s    SDL_Texture;

enum {
    SDL_RENDERER_SOFTWARE = 0,
    SDL_RENDERER_ACCELERATED = 0x1,
    SDL_RENDERER_PRESENTVSYNC = 0x2,
    SDL_RENDERER_TARGETTEXTURE = 0x4,
};

enum {
    SDL_TEXT_ATTRIBUTE_BOLD = 0x1,
    SDL_TEXT_ATTRIBUTE_ITALIC = 0x2,
    SDL_TEXT_ATTRIBUTE_UNDERLINE = 0x4,
    SDL_TEXT_ATTRIBUTE_REVERSE = 0x8,
};

typedef struct SDL_RendererInfo {
    const char *name;
    uint32_t flags;
    uint32_t num_texture_formats;
    uint32_t texture_formats[16];
    int max_texture_width;
    int max_texture_height;
} SDL_RendererInfo;

SDL_Renderer *SDL_CreateRenderer    (SDL_Window *window, int index, uint32_t flags);
int     SDL_CreateWindowAndRenderer     (int w, int h, int flags, SDL_Window **win_out, SDL_Renderer **rend_out);
int     SDL_GetRendererInfo     (SDL_Renderer *, SDL_RendererInfo *);
void    SDL_GetRendererOutputSize(SDL_Renderer *, int *w, int *h);
void    SDL_DestroyRenderer     (SDL_Renderer *);
void    SDL_SetRenderDrawColor  (SDL_Renderer *, int r, int g, int b, int a);
void    SDL_SetRenderDrawBlendMode(SDL_Renderer *, SDL_BlendMode mode);
void    SDL_RenderClear         (SDL_Renderer *);
void    SDL_RenderPresent       (SDL_Renderer *);
void    SDL_RenderCopy          (SDL_Renderer *, SDL_Texture *texture, const SDL_Rect *src, const SDL_Rect *dst);
void    SDL_RenderDrawLines     (SDL_Renderer *, const SDL_Point *points, uint32_t count);
void    SDL_RenderFillRect      (SDL_Renderer *, const SDL_Rect *rect);
void    SDL_RenderDrawRect      (SDL_Renderer *, const SDL_Rect *rect);
void    SDL_RenderSetClipRect   (SDL_Renderer *, const SDL_Rect *clip);

SDL_Texture *SDL_GetRenderTarget(SDL_Renderer *);
void        SDL_SetRenderTarget(SDL_Renderer *, SDL_Texture *texture);

SDL_Texture * SDL_CreateTexture (SDL_Renderer *, int format, int access, int w, int h);
SDL_Texture * SDL_CreateTextureFromSurface(SDL_Renderer *, SDL_Surface *);
void    SDL_DestroyTexture      (SDL_Texture *);
void    SDL_SetTextureBlendMode (SDL_Texture *, SDL_BlendMode mode);
void    SDL_SetTextureColorMod  (SDL_Texture *, uint8_t r, uint8_t g, uint8_t b);
void    SDL_SetTextureAlphaMod  (SDL_Texture *, uint8_t alpha);
int     SDL_QueryTexture        (SDL_Texture *, uint32_t *format, int *access, int *w, int *h);

/* Text rendering routines (not part of regular SDL): */

void    SDL_SetRenderTextColor  (SDL_Renderer *, int r, int g, int b);
void    SDL_SetRenderTextFillColor(SDL_Renderer *, int r, int g, int b, int a);
void    SDL_SetRenderTextAttributes(SDL_Renderer *, int attributes);
void    SDL_RenderDrawUnicode   (SDL_Renderer *, int x, int y, uint32_t ucp); /* single UTF-32 code point */
int     SDL_UnicodeWidth        (SDL_Renderer *, uint32_t ucp);
