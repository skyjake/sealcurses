# SEALCurses: SDL Emulation and Adaptation Layer for Curses

Implementation of selected parts of the SDL API using Curses. Intended to be used as a drop-in replacement for the regular SDL.
